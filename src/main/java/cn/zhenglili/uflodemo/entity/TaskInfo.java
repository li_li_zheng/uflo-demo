package cn.zhenglili.uflodemo.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 实体类
 *
 * @author wanheng
 * @since 2020-10-02 19:16:36
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class TaskInfo implements Serializable {
    private static final long serialVersionUID = -92704064946737426L;
    /**
     * 主键
     */
    @TableId(type = IdType.UUID)
    private String id;
    /**
     * processID
     */
    private Long processInstanceId;
    /**
     * taskID
     */
    private Long taskId;
    /**
     * 表单设计器id
     */
    private String formDesignerId;
    /**
     * online数据表id
     */
    private String onlineTableId;
    /**
     * online数据id
     */
    private String onlineDataId;

}
