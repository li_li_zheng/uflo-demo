package cn.zhenglili.uflodemo.config;

import com.bstek.uflo.console.UfloServlet;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @ClassName：ServletConfig
 * @Description：TODO
 * @Author：zhenglili
 * @Date：2020/9/2 18:17
 **/
@Configuration
public class ServletConfig {

    @Bean
    public ServletRegistrationBean servletRegistration() {
        //为servlet配置路径
        return new ServletRegistrationBean(new UfloServlet(), "/uflo/*");
    }

}
