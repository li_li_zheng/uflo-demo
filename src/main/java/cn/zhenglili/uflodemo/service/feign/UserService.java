package cn.zhenglili.uflodemo.service.feign;

import cn.zhenglili.uflodemo.entity.SysRole;
import cn.zhenglili.uflodemo.utils.Result;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.annotations.ApiOperation;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * 用户体系接口调用
 *
 * @author wanheng
 * @since 2020/10/4
 */
@Service
@FeignClient("USER-SYSTEM")
public interface UserService {

    @RequestMapping(value = "/sys/role/queryall", method = RequestMethod.GET)
    Result<List<SysRole>> queryall();

    @GetMapping("/sys/user/queryUserByRoleId")
    Result<List<String>> queryUserByRoleId(@RequestParam(name = "roleId") String roleId);

}
