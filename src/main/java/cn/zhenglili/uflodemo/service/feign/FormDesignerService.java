package cn.zhenglili.uflodemo.service.feign;

import cn.zhenglili.uflodemo.utils.Result;
import io.swagger.annotations.ApiParam;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

/**
 * @ClassName：FormDesignerService
 * @Description
 * @Author：zhenglili
 * @Date：2020/9/30 23:05
 **/
@Service
@FeignClient("FMAKING")
public interface FormDesignerService {

    @GetMapping("/admin/desform/{id}/getConent")
    Result<?> getConent(@PathVariable @ApiParam("表单ID") String id);
}
