package cn.zhenglili.uflodemo.mapper;

import org.apache.ibatis.annotations.Mapper;
import cn.zhenglili.uflodemo.entity.TaskInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

@Mapper
public interface TaskInfoMapper extends BaseMapper<TaskInfo> {
}
