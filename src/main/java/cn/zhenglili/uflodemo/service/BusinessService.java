package cn.zhenglili.uflodemo.service;

import com.alibaba.fastjson.JSONObject;
import com.bstek.uflo.console.handler.impl.list.AssigneeInfo;
import com.bstek.uflo.model.ProcessDefinition;
import com.bstek.uflo.model.task.Task;
import com.bstek.uflo.process.assign.AssigneeProvider;
import com.bstek.uflo.process.assign.Entity;
import com.bstek.uflo.process.assign.PageQuery;
import org.springframework.context.ApplicationContext;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * @author wanheng
 * @since 2020/10/2
 */

public interface BusinessService {

    /**
     * 获取可选的参与者类型集合
     *
     * @param applicationContext
     * @return
     */
    Map<String, AssigneeProvider> assigneeProviderMap(ApplicationContext applicationContext);

    /**
     * 根据providerKey设置对应人员类型的基本信息
     *
     * @param assigneeProviderMap
     * @param providerKey
     * @return
     */
    AssigneeInfo bulidAssigneeInfo(Map<String, AssigneeProvider> assigneeProviderMap, String providerKey);

    /**
     * 有详细的类型providerKey，获取对应的人员
     *
     * @param assigneeProviderMap
     * @param providerKey
     * @param pageQuery
     * @param keyWord
     * @return
     */
    AssigneeInfo buildAssigneeInfoDetails(Map<String, AssigneeProvider> assigneeProviderMap, String providerKey, PageQuery<Entity> pageQuery, String keyWord);

    /**
     * 查询文件名是否存在，存在为true，不存在为false
     *
     * @param fileName
     * @return
     */
    boolean fileNameIsExist(String fileName);

    /**
     * 删除流程定义文件
     *
     * @param fileName
     */
    void deleteFile(String fileName);

    /**
     * 根据fileName部署流程
     *
     * @param fileName
     */
    void deployProcessByFileName(String fileName);

    /**
     * 更改xml文件name标签值
     *
     * @param fileName
     * @throws TransformerException
     * @throws IOException
     * @throws SAXException
     * @throws ParserConfigurationException
     */
    void changeXmlRootName(String fileName) throws TransformerException, IOException, SAXException, ParserConfigurationException;

    /**
     * 查询已部署的流程的定义信息
     *
     * @param processDefinitionName
     * @param pageNo
     * @param pageSize
     * @return
     */
    List<ProcessDefinition> getProcessDefinitions(String processDefinitionName, int pageNo, int pageSize);

    /**
     * 获取开始节点表单设计
     * @return
     */
    Map<String,Object> getStartFormDesigner(Long processId);

    /**
     * 填写表单后，启动流程
     *
     * @param processId
     * @param userId
     * @param contentJson
     */
    Long startProcess(Long processId, String userId, JSONObject contentJson);

    /**
     * 获取任务节点表单设计
     * @param taskId
     * @return
     */
    Map<String,Object> getTaskFormDesigner(Long taskId);

    /**
     * 获取任务节点表单设计及数据
     * @param taskId
     * @return
     */
    Map<String, Object> getFormDesignerAndData(Long processInstanceId,Long taskId);

    /**
     * 获取当前表单设计及前几个节点表单及数据
     *
     * @param taskId
     * @return
     */
    Map<String,Object> getForm(Long taskId);

    /**
     * 查询用户待处理任务
     * @param userId
     * @return
     */
    List<Task> getTodoTasks(String userId);

    /**
     * 完成任务
     *
     * @param taskId
     * @param contentJson
     */
    void completeTask(Long taskId, JSONObject contentJson);


    String decode(String str);



}
