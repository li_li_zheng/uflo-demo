package cn.zhenglili.uflodemo.utils.constants;

/**
 *  应该用枚举定义
 * Created by Yang on 2017/2/13.
 */
public class BaseMsg {

    public static final int CODE_SUCCESS = 0;//请求/操作成功code
    public static final int CODE_FAILED = 1;//请求/操作失败code
    public static final int CODE_EXCEPTION = -1;//请求/操作异常code

    /**
     * 反馈的提示
     */
    public static final String MSG_JSON_FORMAT_ERROR = "json 格式有误";
    public static final String MSG_SUCCESS = "操作成功！";
    public static final String MSG_DOWNLOAD_SUCCESS = "文件下载成功";
    public static final String MSG_UPLOAD_FILE_SUCCESS = "文件上传成功";
    public static final String ERROR_FILE_NOT_VALID = "抱歉，该文件还未进行安全性检测，暂时还无法下载";
    public static final String MSG_FAILED = "操作失败！";
    public static final String TOKEN_ERROR = "token有误";
    public static final String MSG_NO_DATA = "无数据！";
    public static final String ERROR_TASK_NOT_EXIST = "该任务不存在";
    public static final String MSG_DATA_ERROR = "数据错误";
    public static final String PENDING = "请求中";
    public static final String OVER_WORKLOAD = "工作量超过阈值";
    public static final String TASK_DEALED = "任务已被处理！";
    public static final String MISS_OR_TIMEOUT = "任务不匹配或者任务已超时退回";
    public static final String FILE_NOT_SUPPORT= "文件名称或类型有误！";
    public static final String PROCESS_KEY_EXIST= "当前流程名称对应的key已经存在！";
    public static final String PROCESS_INSTANCE_EXIST= "当前流程已有流程实例，无法删除！";
    public static final String PROCESS_INSTANCE_START= "当前流程已经开启，无法删除！";
    public static final String FILE_NOT_EXIST = "抱歉，该文件不存在或已删除";
    public static final String FILE__EXCEPTION = "抱歉，该文件存在异常";
    public static final String EXCEPTION_MKDIR_ERROR = "创建文件目录失败";
    public static final String FILE_PATH_NOT_A_DIR = "抱歉，配置的文件夹路径不是文件夹";
    public static final String FILE_UPLOAD_ERROR = "抱歉，文件上传失败";
    public static final String FILE_NAME_ERROR = "抱歉，文件名称必须包含后缀名";
    public static final String ERROR_MUST_CHECK_WORKLOAD = "抱歉，该工作量需要进行审核，请从列表中选择指定的管理员";
    public static final String ERROR_REVIEW_TASK_WORKLOAD = "抱歉，该任务的工作量已被审核过或您无权审核";
    public static final String ERROR_BUSINESS_ID_NOT_EXIST = "抱歉，该业务ID不存在";
    public static final String ERROR_CHECK_BANK_CARD = "抱歉，需要您绑定银行卡信息和完善住址信息！";
    public static final String ERROR_FILE_NAME_EXIST = "抱歉，该流程名称已存在";
    public static final String ERROR_RETURN_TASK_NOT_EXIST = "操作失败，该任务已被其他管理员审核过";
    public static final String ERROR_VALID_CALCULATE_NO_WORKLOAD = "请输入有效的工作量信息";
    public static final String ERROR_VALID_CALCULATE_NO_REASON = "请给出计算工作量的原因";
    public static final String ERROR_INVALID_NO_REASON = "请给出无效退回原因";
    public static final String ERROR_WORKLOAD_OVER_STAND = "操作失败，所给工作量超过标准工作量";
    /**
     * 解析table
     */
    public static final String PARSE_FAIL = "解析失败";
    /**
     *
     */
    public static final String TASKTIMEOUT = "任务已超时！";
    /**
     *元数据入库使用
     */
    public static final int CODE_DATAINPUT_FAILED = 2;//数据入元数据库失败
    public static final String MSG_DATAINPUT_FAILED = "数据入元数据库失败";


    public static final String ERROR_WORKLOAD_OVER_MYSQL = "工作量数值过大，数据库字段大小暂不支持";
}
