package cn.zhenglili.uflodemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.ImportResource;
// exclude= HibernateJpaAutoConfiguration.class
// 否则部署时会出现java.lang.ClassCastException:
// org.springframework.orm.jpa.EntityManagerHolder cannot be cast to org.springframework.orm.hibernate5.SessionHolder
@SpringBootApplication(exclude= HibernateJpaAutoConfiguration.class)
@ImportResource(locations = {"classpath:uflo-console-context.xml"})
@EnableDiscoveryClient
@EnableFeignClients
public class UflodemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(UflodemoApplication.class, args);
    }
}
