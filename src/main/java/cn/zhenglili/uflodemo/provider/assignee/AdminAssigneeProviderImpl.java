package cn.zhenglili.uflodemo.provider.assignee;

import com.bstek.uflo.env.Context;
import com.bstek.uflo.model.ProcessInstance;
import com.bstek.uflo.process.assign.AssigneeProvider;
import com.bstek.uflo.process.assign.Entity;
import com.bstek.uflo.process.assign.PageQuery;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * @ClassName：AdminAssigneeProviderImpl
 * @Description：可选的管理员参与者
 * @Author：zhenglili
 * @Date：2020/9/15 18:11
 **/
//@Component
@Slf4j
public class AdminAssigneeProviderImpl implements AssigneeProvider {

    @Override
    public boolean isTree() {
        return false;
    }

    @Override
    public String getName() {
        return "管理员";
    }

    /**
     * 查询可选择的用户放入pageQuery
     * @param pageQuery
     * @param s
     */
    @Override
    public void queryEntities(PageQuery<Entity> pageQuery, String s) {
        List<Entity> entities = new ArrayList<>();
        // 外部接口根据关键字查询人员信息

    }

    @Override
    public Collection<String> getUsers(String s, Context context, ProcessInstance processInstance) {
        return null;
    }

    @Override
    public boolean disable() {
        return false;
    }
}
