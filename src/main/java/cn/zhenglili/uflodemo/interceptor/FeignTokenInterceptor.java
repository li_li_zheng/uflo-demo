package cn.zhenglili.uflodemo.interceptor;

import feign.RequestInterceptor;
import feign.RequestTemplate;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.Enumeration;


/**
 * feign调用服务时，会丢失请求头信息。需要在这里把认证信息收到添加上去
 /**
 * @author wanheng
 * @date 2020/8/10 10:30
 */
@Configuration
@Slf4j
public class FeignTokenInterceptor implements RequestInterceptor {

    public final static String X_ACCESS_TOKEN = "X-Access-Token";



    @Override
    public void apply(RequestTemplate requestTemplate) {
        log.info("======上下文中获取原请求信息======");
        String token = "without token";
        String userId = "without userId";
        HttpServletRequest request = ((ServletRequestAttributes)
                RequestContextHolder.getRequestAttributes()).getRequest();
        Enumeration<String> headerNames = request.getHeaderNames();
        while (headerNames.hasMoreElements()) {
            String headerName = headerNames.nextElement();
            String headValue = request.getHeader(headerName);
            log.info("===原请求头信息=== headName: {}, headValue: {}", headerName, headValue);
            if ("x-access-token".equals(headerName)) {
                token = headValue;
            }
            if ("userid".equals(headerName)) {
                userId = headValue;
            }
        }
        log.info("=======Feign添加头部信息start======");
        requestTemplate.header(X_ACCESS_TOKEN, token);
        requestTemplate.header("userId", userId);
        log.info("=======Feign添加头部信息end======");
    }
}

