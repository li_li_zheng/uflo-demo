package cn.zhenglili.uflodemo.provider;
import cn.zhenglili.uflodemo.utils.SpringContextUtils;
import org.hibernate.SessionFactory;
import org.springframework.transaction.PlatformTransactionManager;
import com.bstek.uflo.env.EnvironmentProvider;

import javax.servlet.http.HttpServletRequest;

/**
 * uflo环境配置
 */
public class EnvironmentProviderImpl implements EnvironmentProvider {

    private SessionFactory sessionFactory;
    private PlatformTransactionManager platformTransactionManager;

    @Override
    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public PlatformTransactionManager getPlatformTransactionManager() {
        return platformTransactionManager;
    }

    public void setPlatformTransactionManager(
            PlatformTransactionManager platformTransactionManager) {
        this.platformTransactionManager = platformTransactionManager;
    }
    @Override
    public String getCategoryId() {
        return null;
    }

    /**
     * 登录用户
     * @return
     */
    @Override
    public String getLoginUser() {
        // 获取登录用户
        HttpServletRequest request = SpringContextUtils.getHttpServletRequest();
        return request.getHeader("username");
    }
}
