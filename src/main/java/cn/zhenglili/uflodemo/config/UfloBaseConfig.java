package cn.zhenglili.uflodemo.config;

import com.bstek.uflo.UfloPropertyPlaceholderConfigurer;

import org.springframework.context.EnvironmentAware;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;

import java.util.Properties;

/**
 * @ClassName：UfloBaseConfig
 * @Description：TODO
 * @Author：zhenglili
 * @Date：2020/9/3 17:48
 **/
@Configuration
public class UfloBaseConfig implements EnvironmentAware {

    /**
     * 获取配置文件信息
     */
    private Environment environment;

    @Override
    public void setEnvironment(Environment environment) {
        this.environment = environment;
    }

    @Bean
    public UfloPropertyPlaceholderConfigurer create() {
        UfloPropertyPlaceholderConfigurer ufloPropertyPlaceholderConfigurer = new UfloPropertyPlaceholderConfigurer();
        Properties properties = new Properties();
        // 将uflo的配置注入jar包中
       // properties.setProperty("uflo.defaultFileStoreDir",environment.getProperty("uflo.defaultFileStoreDir"));
        properties.setProperty("uflo.defaultFileStoreDir","/home/ontoweb/backServer/uflodemo/file/");
        ufloPropertyPlaceholderConfigurer.setProperties(properties);
        ufloPropertyPlaceholderConfigurer.setOrder(1000);
        return ufloPropertyPlaceholderConfigurer;
    }


}
