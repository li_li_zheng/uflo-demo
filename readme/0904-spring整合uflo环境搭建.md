[toc]

## spring 整合uflo

### 一、结构图

<img src="F:\Typora_workplace\images\104212_17b65596_6507749.png" alt="输入图片说明" title="屏幕截图.png" style="zoom:67%;" />

#### 二、代码

##### 1.jar包

```java
    <parent>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-parent</artifactId>
        <version>2.3.3.RELEASE</version>
        <relativePath/> <!-- lookup parent from repository -->
    </parent>
    <groupId>cn.zhenglili</groupId>
    <artifactId>uflodemo</artifactId>
    <version>0.0.1-SNAPSHOT</version>
    <name>uflodemo</name>
    <description>Demo project for Spring Boot</description>

    <properties>
        <java.version>1.8</java.version>
    </properties>

    <dependencies>
        <!-- spring boot -->
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter</artifactId>
        </dependency>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-web</artifactId>
        </dependency>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-devtools</artifactId>
            <scope>runtime</scope>
            <optional>true</optional>
        </dependency>
        <!-- uflo 引擎包 -->
        <dependency>
            <groupId>com.bstek.uflo</groupId>
            <artifactId>uflo-console</artifactId>
            <version>2.0.2</version>
        </dependency>
        <!-- mysql连接 -->
        <dependency>
            <groupId>mysql</groupId>
            <artifactId>mysql-connector-java</artifactId>
        </dependency>
        <!-- jpa -->
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-data-jpa</artifactId>
        </dependency>
```

##### 2.配置

```java
server:
  port: 8080
# mysql
spring:
  datasource:
    url: jdbc:mysql://39.100.115.134:3306/uflo?useUnicode=true&characterEncoding=UTF-8&allowMultiQueries=true
    username: root
    password: 960525
    driver-class-name: com.mysql.cj.jdbc.Driver
  # jpa内嵌hibernate配置
  jpa:
    properties:
      hibernate:
        current_session_context_class: org.springframework.orm.hibernate5.SpringSessionContext
        dialect: org.hibernate.dialect.MySQL5Dialect
        jdbc:
          batch_size: 100
        hbm2ddl:
          auto: update
    database-platform: org.hibernate.dialect.MySQL5InnoDBDialect
    show-sql: true
    generate-ddl: true
```

##### LocalSessionFactoryBeanConfig

```java
package cn.zhenglili.uflodemo.config.database;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;

import javax.annotation.Resource;
import javax.sql.DataSource;
import java.util.Properties;

@Configuration
public class LocalSessionFactoryBeanConfig {
    @Resource
private DataSource dataSource;

/**
 * 数据库 hibernate配置
 * @return
 */
@Bean(name="sessionFactory")
public LocalSessionFactoryBean createSessionFactory(){
    LocalSessionFactoryBean localSessionFactoryBean = new LocalSessionFactoryBean();
    localSessionFactoryBean.setDataSource(dataSource);
    localSessionFactoryBean.setPackagesToScan("com.bstek.uflo.model");
    // hibernate配置,剩下在配置文件jpa中已配置
    Properties properties = new Properties();
    properties.setProperty("hibernate.hbm2ddl.auto","update");
    localSessionFactoryBean.setHibernateProperties(properties);
    return localSessionFactoryBean;
}
}
```

##### TransactionManagerConfig

```java
package cn.zhenglili.uflodemo.config.database;

import cn.zhenglili.uflodemo.provider.EnvironmentProviderImpl;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.orm.hibernate5.HibernateTransactionManager;

@Configuration
public class TransactionManagerConfig {
    @Autowired
    @Qualifier("sessionFactory")
    private SessionFactory sessionFactory;
    @Bean(name="transactionManager")
public HibernateTransactionManager createHibernateTransactionManager(){
    HibernateTransactionManager hibernateTransactionManager = new HibernateTransactionManager();
    hibernateTransactionManager.setSessionFactory(sessionFactory);
    return hibernateTransactionManager;
}

@Bean
public EnvironmentProviderImpl createEnvironmentProviderImpl(){
    EnvironmentProviderImpl environmentProvider = new EnvironmentProviderImpl();
    // 设置EnvironmentProviderImpl的两个变量：sessionFactory transactionManager
    environmentProvider.setSessionFactory(sessionFactory);
    environmentProvider.setPlatformTransactionManager(this.createHibernateTransactionManager());
    return environmentProvider;
}
    }
```

##### ServletConfig

```java
package cn.zhenglili.uflodemo.config;

import com.bstek.uflo.console.UfloServlet;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**

 * @ClassName：ServletConfig

 * @Description：TODO

 * @Author：zhenglili

 * @Date：2020/9/2 18:17
   **/
   @Configuration
   public class ServletConfig {

   @Bean
   public ServletRegistrationBean servletRegistration() {
       //为servlet配置路径
       return new ServletRegistrationBean(new UfloServlet(), "/uflo/*");
   }

}
```

##### UfloBaseConfig

```java
package cn.zhenglili.uflodemo.config;

import com.bstek.uflo.UfloPropertyPlaceholderConfigurer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Properties;

/**

 * @ClassName：UfloBaseConfig

 * @Description：TODO

 * @Author：zhenglili

 * @Date：2020/9/3 17:48
   **/
   @Configuration
   public class UfloBaseConfig {

   @Bean
   public UfloPropertyPlaceholderConfigurer create() {
       UfloPropertyPlaceholderConfigurer ufloPropertyPlaceholderConfigurer = new UfloPropertyPlaceholderConfigurer();
       Properties properties = new Properties();
       // 将uflo的配置注入jar包中
       properties.setProperty("uflo.defaultFileStoreDir","D:/uflo");
       ufloPropertyPlaceholderConfigurer.setProperties(properties);
       ufloPropertyPlaceholderConfigurer.setOrder(1000);
       return ufloPropertyPlaceholderConfigurer;
   }
   }
```

##### EnvironmentProviderImpl

```java
package cn.zhenglili.uflodemo.provider;
import org.hibernate.SessionFactory;
import org.springframework.transaction.PlatformTransactionManager;
import com.bstek.uflo.env.EnvironmentProvider;

/**
 * uflo环境配置
   */
   public class EnvironmentProviderImpl implements EnvironmentProvider {

   private SessionFactory sessionFactory;
   private PlatformTransactionManager platformTransactionManager;

   @Override
   public SessionFactory getSessionFactory() {
       return sessionFactory;
   }

   public void setSessionFactory(SessionFactory sessionFactory) {
       this.sessionFactory = sessionFactory;
   }

   @Override
   public PlatformTransactionManager getPlatformTransactionManager() {
       return platformTransactionManager;
   }

   public void setPlatformTransactionManager(
           PlatformTransactionManager platformTransactionManager) {
       this.platformTransactionManager = platformTransactionManager;
   }
   @Override
   public String getCategoryId() {
       return null;
   }

   /**
    * 登录用户
    * @return
      */
      @Override
      public String getLoginUser() {
      // 获取登录用户
      return "admin";
      }
  }
```



##### UflodemoApplication

```java
package cn.zhenglili.uflodemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ImportResource;

@ImportResource(locations = {"classpath:uflo-console-context.xml"})
@SpringBootApplication
public class UflodemoApplication {
    public static void main(String[] args) {
    SpringApplication.run(UflodemoApplication.class, args);
	}
 }
```





