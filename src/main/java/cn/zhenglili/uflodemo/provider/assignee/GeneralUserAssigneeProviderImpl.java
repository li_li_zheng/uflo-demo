package cn.zhenglili.uflodemo.provider.assignee;

import com.bstek.uflo.env.Context;
import com.bstek.uflo.model.ProcessInstance;
import com.bstek.uflo.process.assign.AssigneeProvider;
import com.bstek.uflo.process.assign.Entity;
import com.bstek.uflo.process.assign.PageQuery;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * @ClassName：GeneralUserAssigneeProviderImpl
 * @Description：可选的普通用户参与者
 * @Author：zhenglili
 * @Date：2020/9/16 10:46
 **/
@Component
@Slf4j
public class GeneralUserAssigneeProviderImpl implements AssigneeProvider {
    @Override
    public boolean isTree() {
        return false;
    }

    @Override
    public String getName() {
        return "普通用户";
    }

    @Override
    public void queryEntities(PageQuery<Entity> pageQuery, String s) {
        List<Entity> entities = new ArrayList<>();
        // 外部接口查询根据角色的用户
        // externalCallService.getUserInfoList("1",pageQuery.getPageIndex(),pageQuery.getPageSize(),keyWord);
        entities.add(new Entity("123","zhenglili"));
        entities.add(new Entity("456","wanheng"));
        pageQuery.setRecordCount(entities.size());
        pageQuery.setResult(entities);
    }

    @Override
    public Collection<String> getUsers(String s, Context context, ProcessInstance processInstance) {
        return null;
    }

    @Override
    public boolean disable() {
        return false;
    }
}
