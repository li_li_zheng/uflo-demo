package cn.zhenglili.uflodemo.config.database;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;

import javax.annotation.Resource;
import javax.sql.DataSource;
import java.util.Properties;

@Configuration
public class LocalSessionFactoryBeanConfig {

    @Resource
    private DataSource dataSource;

    /**
     * 数据库 hibernate配置
     * @return
     */
    @Bean(name="sessionFactory")
    public LocalSessionFactoryBean createSessionFactory(){
        LocalSessionFactoryBean localSessionFactoryBean = new LocalSessionFactoryBean();
        localSessionFactoryBean.setDataSource(dataSource);
        localSessionFactoryBean.setPackagesToScan("com.bstek.uflo.model");
        // hibernate配置,剩下在配置文件jpa中已配置
        Properties properties = new Properties();
        properties.setProperty("hibernate.hbm2ddl.auto","update");
        localSessionFactoryBean.setHibernateProperties(properties);
        return localSessionFactoryBean;
    }

}
