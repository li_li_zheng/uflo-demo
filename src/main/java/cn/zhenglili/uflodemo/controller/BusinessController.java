package cn.zhenglili.uflodemo.controller;

import cn.zhenglili.uflodemo.service.BusinessService;
import cn.zhenglili.uflodemo.utils.Result;
import com.alibaba.fastjson.JSONObject;
import com.bstek.uflo.model.ProcessDefinition;
import com.bstek.uflo.model.task.Task;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

/**
 * @ClassName：BusinessController
 * @Description：TODO
 * @Author：zhenglili
 * @Date：2020/9/4 16:31
 **/
@Slf4j
@RestController
@RequestMapping("/admin/business")
@Api("业务接口")
public class BusinessController {
    @Autowired
    private BusinessService businessService;

    /**
     * 流程部署
     * 受影响表： 1 uflo_blob 2 uflo_process
     *
     * @param fileName
     * @return
     */
    @ApiOperation("通过流程文件名部署流程")
    @PostMapping("deployProcessByFileName")
    public Result deployProcessByFileName(@RequestParam String fileName) {
        log.info("enter into /admin/business/deployProcessByFileName&fileName=" + fileName);
        businessService.deployProcessByFileName(fileName);
        return Result.ok("流程部署成功");
    }

    /**
     * 部署后的流程的定义信息
     * 查询表： uflo_process
     *
     * @param processDefinitionName
     * @param pageNo
     * @param pageSize
     * @return
     */
    @ApiOperation("获取流程的定义信息")
    @GetMapping("getProcessDefinitions")
    public Result getProcessDefinitions(@RequestParam(required = false) String processDefinitionName,
                                        @RequestParam(defaultValue = "1") int pageNo,
                                        @RequestParam(defaultValue = "10") int pageSize) {
        log.info("enter into /admin/business/getProcessDefinitions&processDefinitionName="
                + processDefinitionName + "&pageNo=" + pageNo + "&pageSize=" + pageSize);
        Result result = new Result();
        if (pageNo < 1 || pageSize < 0) {
            log.error("分页参数出错");
            return Result.error("分页参数出错");
        }
        List<ProcessDefinition> processDefinitions =
                businessService.getProcessDefinitions(processDefinitionName, pageNo, pageSize);
        result.setSuccess(true);
        result.setResult(processDefinitions);
        return result;
    }

    /**
     * 获取整个流程的表单设计
     * @param processId
     * @return
     */
    @ApiOperation("获取开始节点表单设计")
    @GetMapping("/getStartFormDesigner/{processId}")
    public Result getStartFormDesigner(@PathVariable("processId") Long processId) {
        Result<Map<String, Object>> result = new Result<>();
        log.info("获取开始节点表单");
        Map<String, Object> startFormDesigner= businessService.getStartFormDesigner(processId);
        result.setResult(startFormDesigner);
        return result;
    }


    /**
     * 启动流程实例
     *
     * @param processId   部署之后就有processId,代表部署的id
     * @param contentJson 表单内容
     * @param request     用户信息
     * @return
     */
    @ApiOperation("启动流程实例")
    @PostMapping("startProcess/{processId}")
    public Result startProcess(@PathVariable("processId") Long processId,
                               @RequestBody JSONObject contentJson,
                               HttpServletRequest request) {
        Result result = new Result();
        log.info("enter into /admin/business/startProcess&processId=" + processId);
        log.info("contentJson:" + contentJson);
        String userId = request.getHeader("userId");
        Long processInstanceId = businessService.startProcess(processId, userId, contentJson);
        result.setResult(processInstanceId);
        return result;
    }

    /**
     * 获取当前表单设计及前几个节点表单及数据
     * @param taskId
     * @return
     */
    @ApiOperation("获取当前表单设计及前几个节点表单及数据")
    @GetMapping("getForm/{taskId}")
    public Result getForm(@PathVariable Long taskId) {
        Result<Map<String, Object>> result = new Result<>();
        log.info("获取当前表单及前几个节点表单及数据");
        Map<String, Object> map = businessService.getForm(taskId);
        result.setResult(map);
        return result;
    }

    @ApiOperation("获取当前表单设计")
    @GetMapping("getTaskFormDesigner/{taskId}")
    public Result getTaskFormDesigner(@PathVariable Long taskId) {
        Result<Map<String, Object>> result = new Result<>();
        log.info("获取当前表单设计");
        Map<String, Object> map = businessService.getTaskFormDesigner(taskId);
        result.setSuccess(true);
        result.setResult(map);
        return result;
    }

    @ApiOperation("获取当前表单设计和数据")
    @GetMapping("getFormDesignerAndData/{processInstanceId}/{taskId}")
    public Result getFormDesignerAndData(@PathVariable Long processInstanceId,@PathVariable Long taskId) {
        Result<Map<String, Object>> result = new Result<>();
        log.info("获取当前表单设计和数据");
        Map<String, Object> map = businessService.getFormDesignerAndData(processInstanceId,taskId);
        result.setSuccess(true);
        result.setResult(map);
        return result;
    }

    /**
     * 查询用户需要处理的任务
     * @param request
     * @return
     */
    @ApiOperation("查询用户需要处理的任务")
    @GetMapping("getTodoTasks")
    public Result<List<Task>> getTodoTasks(HttpServletRequest request){
        Result<List<Task>> result = new Result();
        String userId = request.getHeader("userId");
        List<Task> todoTasks = businessService.getTodoTasks(userId);
        result.setResult(todoTasks);
        return result;
    }


    @ApiOperation("完成任务")
    @PostMapping("completeTask/{taskId}")
    public Result completeTask(@PathVariable("taskId") Long taskId,
                               @RequestBody JSONObject contentJson) {
        log.info("完成任务taskId:{}", taskId);
        log.info("表单数据：{}", contentJson);
        businessService.completeTask(taskId, contentJson);
        return Result.ok("任务已完成");
    }
}
