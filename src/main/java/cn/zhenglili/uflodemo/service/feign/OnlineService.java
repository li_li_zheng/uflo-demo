package cn.zhenglili.uflodemo.service.feign;

import cn.zhenglili.uflodemo.utils.Result;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * @ClassName：OnlineService
 * @Description：TODO
 * @Author：zhenglili
 * @Date：2020/9/30 20:21
 **/
@Service
@FeignClient("ONLINE")
public interface OnlineService {

    /**
     * 保存表单数据
     * @param name
     * @param jsonObject
     * @return
     */
    @PostMapping({"/admin/cgform/api/crazyForm/{name}"})
    Result<?> addCrazyForm(@PathVariable("name") String name, @RequestBody JSONObject jsonObject);

    /**
     * 获取表单数据
     * @param code
     * @param id
     * @return
     */
    @GetMapping({"/admin/cgform/api/form/{code}/{id}"})
    Result<?> getForm(@PathVariable("code") String code, @PathVariable("id") String id);
}
