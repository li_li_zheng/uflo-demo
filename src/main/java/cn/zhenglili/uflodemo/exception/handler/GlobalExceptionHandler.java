package cn.zhenglili.uflodemo.exception.handler;

import cn.zhenglili.uflodemo.utils.BaseResponse;
import cn.zhenglili.uflodemo.utils.Result;
import cn.zhenglili.uflodemo.utils.constants.BaseMsg;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import java.io.IOException;

/**
 * @ClassName：GlobalExceptionHandler
 * @Description：全局异常处理
 * @Author：zhenglili
 * @Date：2020/9/5 9:49
 **/
@Slf4j
@RestControllerAdvice
public class GlobalExceptionHandler {
    @ExceptionHandler(value = RuntimeException.class)
    public Result runtimeExceptionHandler(RuntimeException e){
        log.error("全局异常处理处理器捕获了异常，异常原因"+e);
        return Result.error(e.getMessage());
    }
    @ExceptionHandler(value = Exception.class)
    public Result defaultExceptionHandler(Exception e){
        log.error("全局异常处理处理器捕获了异常，异常原因"+e);
        return Result.error(e.getMessage());
    }

    @ExceptionHandler(value = NullPointerException.class)
    public Result nullPointerExceptionHandler(NullPointerException e){
        log.error("全局异常处理处理器捕获了异常，异常原因"+e);
        return Result.error(e.getMessage());
    }
    @ExceptionHandler(value = IOException.class)
    public Result IOExceptionHandler(IOException e){
        log.error("全局异常处理处理器捕获了异常，异常原因"+e);
        return Result.error(e.getMessage());
    }
    //TransformerException
    @ExceptionHandler(value = TransformerException.class)
    public Result transformerExceptionHandler(TransformerException e){
        log.error("全局异常处理处理器捕获了异常，异常原因"+e);
        return Result.error(e.getMessage());
    }
    // SAXException
    @ExceptionHandler(value = SAXException.class)
    public Result SAXExceptionHandler(SAXException e){
        log.error("全局异常处理处理器捕获了异常，异常原因"+e);
        return Result.error(e.getMessage());
    }
    // ParserConfigurationException
    @ExceptionHandler(value = ParserConfigurationException.class)
    public Result parserConfigurationExceptionHandler(ParserConfigurationException e){
        log.error("全局异常处理处理器捕获了异常，异常原因"+e);
        return Result.error(e.getMessage());
    }
}
